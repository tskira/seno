#include <stdio.h>
#include <tgmath.h>

    long double memo_exponenciacao[12] = {[0 ... 11] = -1.0};

    long double potencia(long double x, int y)
    {
        long double resposta = 0.0;
        if (memo_exponenciacao[y] != -1.0)
        {
            return memo_exponenciacao[y];
        }

        if (y == 0) return 1;
        else
        {   
            int index;
            long double anterior = potencia(x, (y - 1));
            long double resposta = 0.0;
            int tam_x = x;

            for (index = 0; index < tam_x; index++)
            {
                resposta += anterior;
            }
            memo_exponenciacao[y] = resposta;
            return resposta;
            
        }
        
    
    }

    int main()
    {
        int index;
        long double ld = 3.999;
        int ta = ld;
        potencia(3.0,10);
        for (index = 0; index < 12; index++)
        {
            printf("\n%Le", memo_exponenciacao[index]);
        }
        return 0;
    }