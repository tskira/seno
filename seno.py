''' Trampo 1 de MC
    Aluno Thiago Kira
    Ra 78750
    SO: lubuntu 17.04
    python 2.7
    precisa dos pacotes numpy
'''

''' Pi '''
import sys
import numpy as np
import math
from decimal import *
import matplotlib.pyplot as plt

''' Estrutura memo para calcular a potencia '''
memo_exponeciacao = [(0.0) for x in range(12)]
termos_seno = [(0.0) for x in range(6)]
decimals = 10

def potencia(x):
    ''' Funcao para calcular potencia de x elevado a y
        utilizando tecnica de programacao dinamica

        @param:
        x: base

        @return:
        preenche memo_exponeciacao com x^y, y | impar
    '''
    getcontext().prec = decimals
    memo_exponeciacao[1] = (x)
    memo_exponeciacao[2] = (pow(x,2))
    memo_exponeciacao[3] = (x*memo_exponeciacao[2])
    memo_exponeciacao[5] = (x*(pow(memo_exponeciacao[2], 2)))
    memo_exponeciacao[7] = (x*(pow(memo_exponeciacao[3], 2)))
    memo_exponeciacao[9] = (x*(pow((memo_exponeciacao[3]*x), 2)))
    memo_exponeciacao[11] =(x*(pow(memo_exponeciacao[5], 2)))

def termos_serie():
    getcontext().prec = decimals
    termos_seno[0] = (0.0)
    for index in range(39916800):
        termos_seno[0] += (memo_exponeciacao[1])
    termos_seno[1] = 0.0
    for index in range(6652800):
        termos_seno[1] += (memo_exponeciacao[3])
    termos_seno[2] = 0.0
    for index in range(332640):
        termos_seno[2] += (memo_exponeciacao[5])
    termos_seno[3] = 0.0
    for index in range(7920):
        termos_seno[3] += (memo_exponeciacao[7])
    termos_seno[4] = 0.0
    for index in range(110):
        termos_seno[4] += (memo_exponeciacao[9])
    termos_seno[5] =(memo_exponeciacao[11])    

def seno_taylor(angulo):
    getcontext().prec = decimals
    potencia(angulo)
    termos_serie()
    seno = (0.0)
    seno -= (termos_seno[5])
    seno += (termos_seno[4])
    seno -= (termos_seno[3])
    seno += (termos_seno[2])
    seno -= (termos_seno[1])
    seno += (termos_seno[0])
    seno = (seno/(39916800.0))
    return seno

Fs = 180
f = 5
sample = 90
x = np.arange(sample)
y = np.sin(np.pi * x /Fs) - seno_taylor(np.pi * x/Fs)
plt.plot(x, y)
plt.xlabel('angulo')
plt.ylabel('sin(angulo) - seno_taylor(angulo')
plt.show()

print('{} : {} ' .format(np.sin(np.pin/6.0), seno_taylor(np.pi/6.0)))